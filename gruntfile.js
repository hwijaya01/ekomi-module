module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				options: {
					loadPath: require('node-neat').with('node-bourbon')
				},
				files: {
					'css/main.css' : 'scss/main.scss'
				}
			}
		},
		postcss:{
			options: {
				map: true,
				processors: [
					require('autoprefixer')({browsers: ['last 3 version']})
				]
			},
			dist:{
				files:{
					'css/main.css':'css/main.css'
				}
			}
		},
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					'css/main.min.css': ['css/main.css']
				}
			}
		},
		uglify: {
			my_target: {
				files: {
					'js/main.min.js': ['src/*.js']
				}
			}
		},
		watch: {
			css: {
				files: '**/*.scss',
				tasks: ['sass','postcss','cssmin']
			},
			scripts: {
				files: ['src/*.js'],
				tasks: ['uglify']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.registerTask('default',['watch']);
}