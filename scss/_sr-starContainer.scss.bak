/*
 * Star Container for shop review form
 * By: hwijaya@ekomi.de
 * version 1.0.0
*/

.shopReviewStarContainer {
  display: table;
  width: 100%;
  max-width: 866px;
  margin-left: auto;
  margin-right: auto;
  padding-top: 20px;
  padding-bottom: 20px;
  padding-left: 20px;
  padding-right: 20px;
  border: 1px solid $starContainerBorderColor;
  @include box-sizing(border-box);

  .starQuestionText {
    font-size: 1.125em;
    @include bolder-font;
    color: $starTitleFontColor;
  }

}

.starBoxContainer {
  display: table;
  width: 100%;
  padding-top: 13px;
  padding-bottom: 13px;
  padding-left: 8px;
  padding-right: 8px;
  margin-bottom: 1em;
  @include box-sizing(border-box);
  border: 1px solid $starContainerBorderColor;

  div {
    display: table;
    width: 100%;
    text-align: center;
    float: left;
  }

  .starImageContainer {
    margin-bottom: 0.5em;

    img {
      margin-right: 15px;
      cursor: pointer;
      width: 30px;
      height: 28px;

      &:nth-last-of-type(1) {
        margin-right: 0;
      }
    }
  }

  .ratingTextContainer {
    @include bolder-font;
    font-size: 1.875em;
    line-height: 33px;
    color: $starRatingColor;

    span {
      color: $starRatingColorBold;
    }
  }
}

@media (min-width: 768px) {
  .starBoxContainer {
    padding-right: 13px;
    padding-left: 13px;
    max-width: 350px;
    margin-bottom: 0;
    margin-left: auto;
    margin-right: auto;

    .starImageContainer {
      margin-bottom: 0;
      max-width: 227px;
    }

    .ratingTextContainer {
      max-width: 50px;
      margin-left: 28px;
    }
  }

  .starHelptext {
    width: 100%;
    max-width: 300px;
    margin: 0 auto;
  }

  .starContent {
    display: table;
    width: 100%;

    &.star-right {
      .starBoxContainer {
        float: right;
      }

      .starHelptext {
        float: left;
      }
    }

    &.star-left {
      .starBoxContainer {
        float: left;
      }

      .starHelptext {
        float: right;
      }
    }
  }
}

@media (min-width: 960px) {
  .starHelptext {
    max-width: 454px;
  }
}