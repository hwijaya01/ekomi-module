head.ready(function () {
	if (window.jQuery) { /* Make Sure JQuery Is loaded */

		var starImage = '.ratingStarImage',
			fullStarClass = 'fillStar';
			starTextContainer = '.ratingTextContainer';

		function starMouseClick(element) {
			var target = element.attr('for');
			var value = $('#'+target).val();

			element.siblings('.bewertung').val(value);
		}

		function starMouseHover(element){
			var target = element.attr('for');
			var value = $('#'+target).val();

			element.parent().siblings(starTextContainer).find('span').html(value);
			element.prevAll(starImage).addClass(fullStarClass);
			element.addClass(fullStarClass);
			element.nextAll(starImage).removeClass(fullStarClass)
		}

		function starMouseOut(element){
			var selectedRating = element.siblings('.bewertung').val();
			var selectedIndex = parseInt(selectedRating)-1;

			if(selectedRating > 0){
				// already set rating star
				starMouseHover(element.parent().children(starImage+':eq('+selectedIndex+')'));
			}else{
				element.parent().siblings(starTextContainer).find('span').html('0');
				element.removeClass(fullStarClass);
				element.siblings(starImage).removeClass(fullStarClass);
			}
		}

		$(starImage).click(function(){
			starMouseClick($(this));
		});

		$(starImage).hover(function() {
			starMouseHover($(this));
		}, function() {
			starMouseOut($(this));
		});
	} else {
		/* if jquery isnt loaded */
	}
});